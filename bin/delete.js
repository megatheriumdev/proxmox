var Proxmox = require('../index.js'),
	async = require('async'),
	config = require('../config.json'),
	next = function(err) {
		if (err) {
			console.error(err);
			throw err;
		}

		console.log('Successfully deleted machines '+JSON.stringify(machineIds)+' within '+(Date.now()-t0)+'ms');
	},
	t0 = Date.now();

var machineIds = process.argv.slice(2);
if (machineIds.length < 0) return next('Sie müssen einen VM-Namen angeben: node bin\\delete.js lamp-micro5');

var prox = new Proxmox(config.host, config.username, config.password);
prox.getNode('prox2', function(err, node) {
	if (err) return next(err);

	console.log('Entered node '+node.id);
	var finished = 0;
	async.eachSeries(machineIds, function(machineId, next) {
		node.getVirtualMachine(machineId, function(err, machine) {
			if (err) return next(err);
			console.log('Deleting '+machineId+'... '+(finished+1)+'/'+machineIds.length);
			machine.delete(function(err) {
				if (err) return next(err);

				++finished;
				next();
			});
		});
	}, next);
});