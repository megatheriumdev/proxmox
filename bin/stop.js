var Proxmox = require('../index.js'),
	config = require('../config.json'),
	next = function(err) {
		if (err) throw err;

		console.log('Successfully stopped machine '+machineId+' within '+(Date.now()-t0)+'ms');
	},
	t0 = Date.now();

var machineId = process.argv[2];
if (!machineId) return next('Sie müssen einen VM-Namen angeben: node bin\\start.js lamp-micro5');

var prox = new Proxmox(config.host, config.username, config.password);
prox.getNode('prox2', function(err, node) {
	if (err) return next(err);

	console.log('Entered node '+node.id);
	node.getVirtualMachine(machineId, function(err, machine) {
		if (err) return next(err);

		console.log('Machine loaded: '+machine.name);
		machine.getStatus(function(err, status) {
			if (err) return next(err);

			console.log('Status: '+status.status);
			if (status.status == 'stopped') return next();
			machine.stop(function(err, taskId) {
				if (err) return next(err);

				console.log('Stopping machine... taskId='+taskId);
			}, next);
		});
	});
});