var Proxmox = require('../index.js'),
	config = require('../config.json'),
	next = function(err) {
		if (err) throw err;

		console.log('Cloning successfully finished within '+(Date.now()-t0)+'ms');
	},
	t0 = Date.now();

var template = process.argv[2];
var cloneName = process.argv[3];
if (!template || !cloneName) return next('Sie müssen ein Template und einen VM-Namen angeben: node bin\\start.js lamp-micro-template-local lamp-micro8');

var prox = new Proxmox(config.host, config.username, config.password);
prox.getNode('prox2', function(err, node) {
	if (err) return next(err);

	console.log('Entered node '+node.id);
	console.log('cloning '+template+' to '+cloneName);
	node.clone(template, {full: true, storage: 'VMs', name: cloneName}, function(err, taskId) {
		if (err) return next(err);

		console.log('Started cloning, taskId='+taskId);
	}, next);
});