var Api = require('./proxmox-api.js'),
	async = require('async');

var Proxmox = function(host, username, password) {
	this.proxmox = new Api(host, 'pam', username, password);
};

Proxmox.prototype.getNode = function(id, next) {
	var node = new Node(id, this.proxmox);
	node.getVirtualMachines(function(err) {
		if (err) return next(err);

		next(undefined, node);
	});
};

Proxmox.prototype.getNodes = function(next) {
	this.proxmox.get('/nodes/', function(err, data) {
		if (err) return next(err);

		next(undefined, data);
	});
};

/**
 * Node
 */
var Node = function(id, proxmox) {
	this.id = id;
	this.proxmox = proxmox;
	this.machines = [];
	this.machineIds = {};
	this.storages = null;
}

Node.prototype.clone = function(name, options, started, next) {
	console.log('clone!');

	if (typeof options === 'function') {
		finished = next;
		next = options;
		options = {};
	}
	if (!next) {
		next = started;
		started = null;
	}

	// setze Optionen
	var clone = function() {
		console.log('started cloning');

		// überprüfe Existenz der IDs
		if (!this.machineIds.hasOwnProperty(name)) return next('Clone: Machine "'+name+'" was not found.');
		if (options.storage && !this.storages.hasOwnProperty(options.storage)) return next('Storage "'+options.storage+'" was not found.');
		if (options.name && this.machineIds.hasOwnProperty(options.name)) return next('Machine "'+options.name+'" already exists.'); 

		// erstelle Anfrage
		var data = {},
			id = this.machineIds[name],
			newId = this.getNextMachineId();
		data.newid = newId;
		if (options.storage) data.storage = options.storage;
		if (options.full) data.full = 1;
		else data.full = 0;
		if (options.name) data.name = options.name;

		// speichere machineId
		this.machineIds[options.name] = newId;

		// führe Clone-Anfrage aus
		this.proxmox.post('/nodes/'+this.id+'/qemu/'+id+'/clone', data, function(err, taskId) {
			if (err) return next(err);

			// registriere Event-Listener
			this.onTaskFinished(taskId, next);

			if (started) started(undefined, taskId);
		}.bind(this));
	}.bind(this);

	if (this.storage != null) return clone();
	this.getStorages(function(err) {
		if (err) return next(err);

		clone();
	});
};

Node.prototype.getNetworks = function(next) {
	this.networks = {};
	this.proxmox.get('/nodes/'+this.id+'/network', function(err, networks) {
		if (err) return next(err);

		for (var i = 0; i < networks.length; ++i) {
			this.networks[networks[i].iface] = networks[i];
		}
		next(undefined, networks);
	}.bind(this));
}

Node.prototype.getStorages = function(next) {
	this.proxmox.get('/nodes/'+this.id+'/storage', function(err, storages) {
		if (err) return next(err);

		this.storages = {};
		for (var i = 0; i < storages.length; ++i) {
			this.storages[storages[i].storage] = storages[i];
		}

		next(undefined, storages);
	}.bind(this));
};

Node.prototype.getTaskStatus = function(taskId, next) {
	this.proxmox.get('/nodes/'+this.id+'/tasks/'+taskId+'/status', next);
}

Node.prototype.getTasks = function(next) {
	this.proxmox.get('/nodes/'+this.id+'/tasks', next);
}

Node.prototype.getVirtualMachine = function(id, next) {
	var findOne = function(next) {
		var machine = null;
		for (var i = 0; i < this.machines.length; ++i)
			if (this.machines[i].name == id) {
				machine = this.machines[i];
				break;
			}

		if (!machine) return next('Machine not found: "'+id+'"');
		machine = new Machine(this, machine, this.proxmox);
		next(undefined, machine);
	}.bind(this);

	findOne(function(err, result) {
		if (err && err.indexOf('Machine not found') == 0) return this.getVirtualMachines(function(err) {
			if (err) return next(err);

			console.log('loaded machines, retry...');

			findOne(next);
		});
		else if (err) return next(err);

		next(undefined, result);
	}.bind(this));
};

Node.prototype.getVirtualMachines = function(next) {
	this.proxmox.get('/nodes/'+this.id+'/qemu/', function(err, data) {
		if (err) return next(err);
		if (!data) return next('Node "'+this.id+'" was not found.');

		this.machines = data;
		this.machineIds = {};
		for (var i = 0; i < data.length; ++i) {
			this.machineIds[data[i].name] = data[i].vmid;
		}

		next(undefined, data);
	}.bind(this));
};

Node.prototype.getNextMachineId = function() {
	var highest = 0;
	for (var i = 0; i < this.machines.length; ++i) {
		if (this.machines[i].vmid > highest) highest = this.machines[i].vmid;
	}
	return highest+1;
};

Node.prototype.getNextMachineName = function(template) {
	var baseName = template.substring(0, template.indexOf('-template')),
		i = 1,
		name = baseName+i;
	while (this.hasMachineName(name)) {
		++i;
		name = baseName+i;
	}

	return name;
};

Node.prototype.getTemplateName = function(name) {
	name = name.replace(/[0-9]+$/, '');
	name += '-template';
	return name;
}

Node.prototype.hasMachineName = function(name) {
	for (var i = 0; i < this.machines.length; ++i) {
		if (name == this.machines[i].name) return true;
	}
	return false;
};

Node.prototype.onTaskFinished = function(taskId, next) {
	if (!taskId) return next('Benötige eine taskId für onTaskFinished, nicht: '+taskId);

	var interval;
	var checkFinished = function() {
		this.getTaskStatus(taskId, function(err, status) {
			if (err) {
				clearInterval(interval);
				return next(err);
			}

			if (status.status == 'running') return;
			clearInterval(interval);
			if (status.exitstatus == 'OK') return next();
			next(status);
		});
	}.bind(this);
	interval = setInterval(checkFinished, 1000);
}


var Machine = function(node, data, proxmox) {
	this.id = data.vmid;
	this.name = data.name;
	this.proxmox = proxmox;
	this.data = data;
	this.node = node;
	this.nodeId = this.node.id;
	this.urlPrefix = '/nodes/'+this.nodeId+'/qemu/'+this.id+'/';
};

Machine.prototype.convertToTemplate = function(name, next) {
	if (!next) {
		next = name;
		name = this.node.getTemplateName(this.name);
	}

	this.proxmox.post(this.urlPrefix+'template', function(err, data) {
		if (err) return next(err);

		console.log(data);
		next();
	});
};

Machine.prototype.delete = function(started, next) {
	if (!next) {
		next = started;
		started = null;
	}

	async.parallel([
		// fahre VM zunächst herunter
		function(next) {
			this.getStatus(function(err, status) {
				if (err) return next();

				if (status.status == 'stopped') return next();
				this.shutdown(next);
			}.bind(this));
		}.bind(this)
	], function(err) {
		if (err) return next(err);

		// dann lösche sie
		this.proxmox.delete(this.urlPrefix, function(err, taskId) {
			if (err) return next(err);

			this.node.onTaskFinished(taskId, next);
			if (started) started(undefined, taskId);
		}.bind(this));
	}.bind(this));
}

Machine.prototype.getStatus = function(next) {
	this.proxmox.get(this.urlPrefix+'status/current', function(err, data) {
		if (err) return next(err);

		next(undefined, data);
	});
}

Machine.prototype.restart = function(next) {
	this.shutdown(function(err) {
		if (err) return next(err);
	}, function(err) {
		if (err) return next(err);

		this.start(function(err) {
			if (err) next(err);
		}, next);
	}.bind(this));
}

Machine.prototype.shutdown = function(started, next) {
	if (!next) {
		next = started;
		started = null;
	}

	this.proxmox.post(this.urlPrefix+'status/shutdown', {forceStop: 1, timeout: 300}, function(err, taskId) {
		if (err) return next(err);

		this.node.onTaskFinished(taskId, next);
		if (started) started(undefined, taskId);
	}.bind(this));
};

Machine.prototype.start = function(started, next) {
	if (!next) {
		next = started;
		started = null;
	}

	this.proxmox.post(this.urlPrefix+'status/start', function(err, taskId) {
		if (err) return next(err);

		this.node.onTaskFinished(taskId, next);
		if (started) started(undefined, taskId);
	}.bind(this));
};

Machine.prototype.stop = function(started, next) {
	if (!next) {
		next = started;
		started = null;
	}

	this.proxmox.post(this.urlPrefix+'status/stop', function(err, taskId) {
		if (err) return next(err);

		this.node.onTaskFinished(taskId, next);
		if (started) started(undefined, taskId);
	}.bind(this));
}

module.exports = Proxmox;