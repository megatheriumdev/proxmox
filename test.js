var async = require('async'),
	colors = require('colors'),
	Proxmox = require('./index.js');

var prox = new Proxmox('yourip', 'root', 'somepass');

var fetchCpu = function(next) {
	prox.getNodes(function(err, nodes) {
		if (err) return next(err);

		prox.getNode('prox2', function(err, node) {
			if (err) return next(err);

			async.each(node.machines, function(machine, next) {
				node.getVirtualMachine(machine.name, function(err, vm) {
					if (err) return next(err);

					var c = vm.data.cpu,
						l = console.log,
						m = vm.data.cpu+'% CPU: '+machine.name;
					if (c > 80) l(m.red);
					else if (c > 50) l(m.yellow);
					else if (c > 1) l(m.green);
					else l(m.gray);
					next();
				});
			}, function(err) {
				if (err) return next(err);

				setTimeout(fetchCpu, 1000);
			});
		});
	});
};
fetchCpu(function(err) {
	if (err) throw err;

	console.log('Finished');
});