var Proxmox = require('./index.js'),
	colors = require('colors'),
	config = require('./config.json'),
	next = function(err) {if (err) throw err; console.log('Finished');};

var prox = new Proxmox(config.host, config.username, config.password);

prox.getNodes(function(err, nodes) {
	if (err) return next(err);

	prox.getNode('prox2', function(err, node) {
		if (err) return next(err);

		console.log(node.machineIds);
		node.getStorages(function(err, storages) {
			if (err) return next(err);


			node.getTasks(function(err, tasks) {
				if (err) return next(err);

				for (var i = 0; i < tasks.length; ++i) {
					var task = tasks[i];
					var time = new Date(task.starttime*1000);
					var message = time.toString()+' '+task.node+'/'+task.id+' - ['+task.type+'] '+task.status;
					if (task.status == 'OK') console.log(message.green);
					else console.log(message.red);
				}
				
				node.getNetworks(function(err, networks) {
					if (err) return next(err);

					console.log('Networks', Object.keys(node.networks));
					next();
				});
			});
		});
	});
});